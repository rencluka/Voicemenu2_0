import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Style {

    // customize button by parameters
    private static void customizeButton(JButton button, Border border, Dimension dim, Color color) {
        button.setBackground(color);
        button.setOpaque(true);
        button.setPreferredSize(dim);
        button.setBorder(border);
    }

    //add listeners for Call and End buttons
    private static void addListeners(JButton Call, JButton End) {
        Call.addActionListener(e -> {
            System.out.println("Calling");
        });
        End.addActionListener(e -> {
            System.out.println("Terminating program");
            //end program
            System.exit(0);
        });
    }

    //set call buttons panel
    private static void setCallButtons(JPanel panel) {

        Border border = new LineBorder(Color.white, 22);
        Border emptyBorder = new LineBorder(Color.white, 10);
        Dimension dim = new Dimension(78, 50);

        //create new buttons
        JButton buttonCall = new JButton("");
        JButton empty = new JButton("HOME");
        JButton buttonEnd = new JButton("");

        //customize buttons look
        customizeButton(buttonCall, border, dim, Color.GREEN);
        customizeButton(empty, emptyBorder, dim, Color.lightGray);
        customizeButton(buttonEnd, border, dim, Color.RED);

        addListeners(buttonCall, buttonEnd);

        //add to pane
        panel.add(buttonCall, BorderLayout.LINE_START);
        panel.add(empty, BorderLayout.CENTER);
        panel.add(buttonEnd, BorderLayout.LINE_END);
    }

    // set deliminator line of the phone
    private static void setDeliminatorLine(JPanel panel){
        JPanel topLine = new JPanel();
        topLine.setBackground(Color.lightGray);
        topLine.setPreferredSize(new Dimension(80, 4));
        panel.setBorder(new EmptyBorder(0,0,0,0));

        JPanel line2 = new JPanel(new GridLayout(1, 3, 0, 0));
        setCallButtons(line2);
        line2.setBackground(Color.white);
        //line2.setPreferredSize(new Dimension(80, 50));

        JPanel botLine = new JPanel();
        botLine.setBackground(Color.lightGray);
        botLine.setPreferredSize(new Dimension(80, 1));


        panel.add(topLine, BorderLayout.NORTH);
        panel.add(line2, BorderLayout.CENTER);
        panel.add(botLine, BorderLayout.SOUTH);

    }

    //set header of the phone
    private static void setHeader(JPanel panel){
        JTextPane header = new JTextPane();
        Insets inset = new Insets(0, 0, 0, 5);
        header.setPreferredSize(new Dimension(80, 15));
        header.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        header.setEditable(false);
        header.setText("10:00");
        header.setMargin(inset);
        header.setBackground(Color.lightGray);
        panel.add(header);
    }

    // set content od the screen
    private static void setScreen(JTextArea myScreen){

        myScreen.setFont(new Font("Arial", Font.PLAIN, 16));
    }

    //set Top Panel content
    private static void setTopPanel(JPanel topPanel){
        JPanel myHeader = new JPanel(new BorderLayout(0,0));
        JTextArea myScreen = new JTextArea("",3,8);

        //set content
        setScreen(myScreen);
        setHeader(myHeader);

        //add componenets to panel
        topPanel.add(myHeader, BorderLayout.NORTH);
        topPanel.add(myScreen, BorderLayout.SOUTH);
    }

    //set MainPanel
    private static void setMainPanel(JPanel myPhone, Main instance){
        JPanel topPanel = new JPanel(new BorderLayout(0,0));
        JPanel myLine = new JPanel(new BorderLayout(0,0));
        JPanel myPanelOfButtons = new JPanel(new GridLayout(4,3,2,2));

        //set top panel + screen
        setTopPanel(topPanel);

        //set deliminator line
        setDeliminatorLine(myLine);

        //set buttons layout
        Data.addButtons(instance,myPanelOfButtons);

        //add components to myPhone panel
        myPhone.add(topPanel, BorderLayout.NORTH);
        myPhone.add(myLine, BorderLayout.CENTER);
        myPhone.add(myPanelOfButtons,BorderLayout.SOUTH);
    }

    //set content of the frame
    public static void setContent(Main instance) {
        JFrame frame = new JFrame("JetPhone");
        JPanel myPhone = new JPanel(new BorderLayout(5,0));

        //set visible content
        setMainPanel(myPhone, instance);

        //add frame
        frame.setContentPane(myPhone);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}