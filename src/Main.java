import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class Main implements ActionListener {

    //Declaring event
    static class Event {
        private String name;
        private String trigger;
        private List<Event> childs;
        private String action;
        private String toast;
    }

    //Hashing map used for a quick search of possible following Events
    private static HashMap<String, Event> myHashMap = new HashMap<>();

    //List used for saving possible next character pressed on the keyboard
    private static List<String> possibleOptions = new ArrayList<>();

    //String representing where we are currently nested. used for backtracking if back option is selected.
    private static String path = "0";

    //Method to initialize hashing map. Has to be called as one of the first things in the main before any number is pressed. Includes declarations of all events.
    private static void initHashMap() {

        Event Home = new Event();
        Home.name = "Home";
        Home.toast = "Welcome to the Home Menu:";
        Home.childs = new ArrayList<>();
        Home.action = "";

        Event Back = new Event();
        Back.name = "back";
        Back.trigger = "#";
        Back.toast = "Going back to the previous menu";
        Back.action = "back";

        Event Car = new Event();
        Car.name = "Buying a car";
        Car.trigger = "2";
        Car.toast = "You've chosen buying a new car";
        Car.childs = new ArrayList<>();
        Car.childs.add(Back);
        Car.action = "";

        Event Technical = new Event();
        Technical.name = "Technical questions";
        Technical.trigger = "1";
        Technical.toast = "What's up man?";
        Technical.childs = new ArrayList<>();
        Technical.action = "";

        Event Internet = new Event();
        Internet.name = "Internet";
        Internet.trigger = "1";
        Internet.toast = "Welcome to the Internet";
        Internet.childs = new ArrayList<>();
        Internet.childs.add(Back);
        Internet.action = "";

        Event Television = new Event();
        Television.name = "problem with television";
        Television.trigger = "2";
        Television.toast = "Welcome to the televsion part";
        Television.childs = new ArrayList<>();
        Television.childs.add(Back);
        Television.action = "";

        Event Discount = new Event();
        Discount.name = "Discount";
        Discount.trigger = "4";
        Discount.toast = "How much do you want to save?";
        Discount.childs = new ArrayList<>();
        Discount.childs.add(Back);
        Discount.action = "";

        Event Increase = new Event();
        Increase.name = "increasing the speed of internet";
        Increase.trigger = "9";
        Increase.toast = "Let's increase the speed";
        Increase.childs = new ArrayList<>();
        Increase.childs.add(Back);
        Increase.action = "";

        Event Big = new Event();
        Big.name = "Big Discount";
        Big.trigger = "7";
        Big.toast = "Good choice!";
        Big.childs = new ArrayList<>();
        Big.childs.add(Back);
        Big.action = "";

        Event Small = new Event();
        Small.name = "Small Discount";
        Small.trigger = "8";
        Small.toast = "I guess you are well off";
        Small.childs = new ArrayList<>();
        Small.childs.add(Back);
        Small.action = "";


        Home.childs.add(Technical);
        Home.childs.add(Car);

        Technical.childs.add(Internet);
        Technical.childs.add(Television);

        Internet.childs.add(Increase);
        Internet.childs.add(Discount);

        Discount.childs.add(Small);
        Discount.childs.add(Big);

        //filling the Hashing map
        myHashMap.put("01", Technical);
        myHashMap.put("02", Car);
        myHashMap.put("011", Internet);
        myHashMap.put("012", Television);
        myHashMap.put("0119", Increase);
        myHashMap.put("0114", Discount);
        myHashMap.put("01148", Small);
        myHashMap.put("01147", Big);
        myHashMap.put("0", Home);

        myHashMap.put("01#", Back);
        myHashMap.put("02#", Back);
        myHashMap.put("011#", Back);
        myHashMap.put("012#", Back);
        myHashMap.put("0119#", Back);
        myHashMap.put("0114#", Back);
        myHashMap.put("01148#", Back);
        myHashMap.put("01147#", Back);

    }


    public static void main(String[] args) {

        //Initilization of hashing map, generating all Events, filling the hashing map
        initHashMap();

        //Static first iteration of Voicemenu

        //declaring first possible following characters
        possibleOptions.add("1");
        possibleOptions.add("2");

        //reference to itself to be later used in ActionListener
        Main instance = new Main();

        //Setting up the style, preparing graphics
        Style.setContent(instance);

        //Itialization of voice output
        VoiceManager vm = VoiceManager.getInstance();
        Voice myVoice = vm.getVoice("kevin16");
        myVoice.allocate();

        //Greetings of Home menu
        myVoice.speak("Welcome to the Home Menu: For technical questions choose 1, for buying a new car press 2");

    }

    //Action Listener
    @Override
    public void actionPerformed(ActionEvent evt) {

        //Initilization of voice output
        VoiceManager vm = VoiceManager.getInstance();
        Voice myVoice = vm.getVoice("kevin16");
        myVoice.allocate();

        //Variable to store which character was pressed
        String character = evt.getActionCommand();

        //Checking if correct option was pressed. If so path is updated
        if (!possibleOptions.contains(character)) {
            myVoice.speak(character + "is a bad option. please try again");
        } else {
            path = path + character;
        }
        System.out.println(path);
        //Loading next Event according to what is specified in "path"
        Event currentEvent = myHashMap.get(path);

        //Checking if "back" option was selected via name of the current event
        if (!currentEvent.action.isEmpty()) {
            if (currentEvent.action.equals("back")) {

                //updating path to get back
                myVoice.speak("Going to the previous menu");
                path = path.substring(0, path.length() - 2);
                //loading previous event
                currentEvent = myHashMap.get(path);
            }
        }

        //Handling voice output
        myVoice.speak(currentEvent.toast);
        myVoice.speak("Choose from");

        //Delete all the previous possible options
        possibleOptions.clear();

        //Proposing possible options consisting of next events
        for (Event child : currentEvent.childs) {
            myVoice.speak("For" + child.name + "press" + child.trigger);
            possibleOptions.add(child.trigger);
        }


    }

}