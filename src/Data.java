import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;

public class Data {

    //list of buttons
    public enum Status {

        BUTTON_1("1"),
        BUTTON_2("2"),
        BUTTON_3("3"),
        BUTTON_4("4"),
        BUTTON_5("5"),
        BUTTON_6("6"),
        BUTTON_7("7"),
        BUTTON_8("8"),
        BUTTON_9("9"),
        STAR("*"),
        BUTTON_0("0"),
        HASH("#");


        private String code;

        Status(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name();
        }
    }

    //method to add buttons to panel
    public static void addButtons(Main instance, JPanel myPanelOfButtons) {

        for (Data.Status item : Data.Status.values()) {
            JButton button = new JButton(item.getCode());
            button.setActionCommand(item.getCode());
            button.addActionListener(instance);
            button.setPreferredSize(new Dimension(78, 76));
            Border border = new LineBorder(Color.white, 13);
            button.setBorder(border);
            myPanelOfButtons.add(button);
        }
    }
}
